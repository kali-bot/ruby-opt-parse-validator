# frozen_string_literal: true

# Gem Version
module OptParseValidator
  VERSION = '1.9.1'
end
